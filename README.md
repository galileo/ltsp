


auto eth1
iface eth1 inet static
  address 192.168.0.1
  netmask 255.255.255.0
  broadcast 192.168.0.255
  gateway 192.168.0.1


# Default LTSP dhcpd.conf config file.
authoritative;

subnet 192.168.0.0 netmask 255.255.255.0 {
    range 192.168.0.20 192.168.0.250;
    option domain-name "example.com";
    option domain-name-servers 192.168.0.1;
    option broadcast-address 192.168.0.255;
    option routers 192.168.0.1;
    next-server 192.168.0.1;
#    get-lease-hostnames true;
    option subnet-mask 255.255.255.0;
    option root-path "/opt/ltsp/i386";
    if substring( option vendor-class-identifier, 0, 9 ) = "PXEClient" {
        filename "/ltsp/i386/pxelinux.0";
    } else {
        filename "/ltsp/i386/nbi.img";
    }
}

$ cat /etc/default/isc-dhcp-server 

# Defaults for isc-dhcp-server initscript
# sourced by /etc/init.d/isc-dhcp-server
# installed at /etc/default/isc-dhcp-server by the maintainer scripts

#
# This is a POSIX shell fragment
#

# Path to dhcpd's config file (default: /etc/dhcp/dhcpd.conf).
#DHCPD_CONF=/etc/dhcp/dhcpd.conf

# Path to dhcpd's PID file (default: /var/run/dhcpd.pid).
#DHCPD_PID=/var/run/dhcpd.pid

# Additional options to start dhcpd with.
#	Don't use options -cf or -pf here; use DHCPD_CONF/ DHCPD_PID instead
#OPTIONS=""

# On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
#	Separate multiple interfaces with spaces, e.g. "eth0 eth1".
INTERFACES="eth1"
Le DHCP distribue ses adresse pour les clients sur l’interface eth1 afin de ne pas entrer en conflit avec le DHCP de la BOX.
 v. Configuration LTSP


$ cat /etc/ltsp/ltsp-update-image.excludes 

cdrom/*
etc/epoptes/server.key
etc/mysql/debian.cnf
etc/NetworkManager/system-connections/*
etc/ssh/ssh_host_*_key
etc/udev/rules.d/??-persistent-*.rules
home/*
lost+found/*
media/*
mnt/*
opt/ltsp*
proc/*
root/.*
root/*
run/*
srv/*
tmp/.*
tmp/*
var/backups/*
var/cache/apt/archives/*.deb
var/cache/apt/archives/partial/*
var/cache/apt-xapian-index/*
var/cache/lightdm/dmrc/*
var/crash/*
var/lib/apt/lists/*
var/lib/lightdm/.*
var/lib/lightdm/*
var/lib/mysql/*
var/lib/sudo/*
var/log/*.1
var/log/*.gz
var/log/*.old
var/mail/*
var/spool/squid3/*
var/tmp/.*
var/tmp/*
 vi. Configuration TFTP
$ cat /etc/default/tftpd-hpa 

# /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/var/lib/tftpboot -v -v -v"
TFTP_ADDRESS=":69"
TFTP_OPTIONS="—secure"

L’option « -v -v -v » peut être utilisée pour diagnostiquer le service TFTP, elle doit être supprimée en mode production.
 
2. Clients

 1. Construction de l’image

sudo ltsp-build-client --exclude software-properties-gtk --arch i386 --purge-chroot –fat-client 

l’option « --exclude software-properties-gtk » n’est pas obligatoire mais permet d’empêcher l’installation de certains logiciels sur le client.

2. Reconfiguration du réseau

L’utilisation du mode FATCLIENT oblige à transformer le serveur en routeur afin que les clients puissent accéder à internet

 i. Schéma de principe

 ii. reconfiguration du DHCP
Le réseau 192.168.0.0/24 est celui du serveur LTSP distribué aux clients
Le réseau 192.168.1.0/24 est celui de la BOX
La BOX (192.168.1.1) doit être désignée comme DNS du réseau 192.168.0.0/24 en utilisant le DHCP :
$ cat /etc/ltsp/dhcpd.conf 

# Default LTSP dhcpd.conf config file.
authoritative;

subnet 192.168.0.0 netmask 255.255.255.0 {
    range 192.168.0.20 192.168.0.250;
    option domain-name "example.com";
    option domain-name-servers 192.168.1.1; # IP de la Livebox
    option broadcast-address 192.168.0.255;
    option routers 192.168.0.1; #IP du serveur LTSP
    next-server 192.168.0.1;
#   get-lease-hostnames true;
    option subnet-mask 255.255.255.0;
    option root-path "/opt/ltsp/i386";
    if substring( option vendor-class-identifier, 0, 9 ) = "PXEClient" {
        filename "/ltsp/i386/pxelinux.0";
    } else {
        filename "/ltsp/i386/nbi.img";
    }
}
 iii. IP forwarding
Afin d’autoriser les clients à utiliser le réseau 192.168.1.0/24, il faut utiliser le mode IP FORWARDING
'### /etc/sysctl.conf
	net.ipv4.ip_forward=1 
Pour une application immédiate
sudo sysctl -w net.ipv4.ip_forward=1
 iv. iptables
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
sudo sh -c "iptables-save > /etc/ltsp/nat"
Application dans le fichier /etc/network/interfaces
auto eth1
iface eth1 inet static
	address 192.168.0.1
	netmask 255.255.255.0
	broadcast 192.168.0.255
	network 192.168.0.0
	up iptables-restore < /etc/ltsp/nat
Sur un système de test, cette configuration a permis d’exécuter les applications sur les postes client tout en conservant la possibilité de se connecter à internet.
L’occupation mémoire est d’environ 1,4Go avec Firefox et LibreOffice ouverts simultanément.
 F. Reconfiguration des clients
Le paramétrage des clients est réalisée avec le fichier lts.conf.
cat /var/lib/tftpboot/ltsp/i386/lts.conf

[default]
	X_MODE_0 = 800x600
	FAT_RAM_THRESHOLD=300

[ce:55:0c:04:14:9a]
	HOSTNAME=poste1

[EE:07:6D:E3:50:9F]
	HOSTNAME=poste2
	X_MODE_0 = 1024x768
	X_COLOR_DEPTH=16
	LOCAL_APPS_MENU=True

Ce fichier peut contenir de nombreux paramètres (voir man lts.conf)
Ici nous nous contentons de définir :
X_MODE_0  : la résolution d’écran par défaut 
FAT_RAM_THRESHOLD : limite entre thin et fat clients
HOSTNAME : le nom de la machine
X_COLOR_DEPTH : nombre de couleur à l’écran
LOCAL_APPS_MENU : permet de forcer l’execution des applications en locale
 G. Installation de logiciels
Ce paragraphe décrit la méthode pour ajouter ou supprimer des programme à l’image du client.
sudo ltsp-chroot

A partir de cet instant toutes les commandes sont exécutés sur le client et non sur le serveur.
apt-get update
apt-get upgrade

sudo apt-get install libreoffice-l10n-fr
sudo apt-get install libreoffice-help-fr

exit

La commande exit permet de quitter la configuration du client et de retourner sur la console du serveur.
sudo ltsp-update-image

A la fin de cette commande, une nouvelle image a été créée en prenant en compte les changements.
ll /opt/ltsp/images/
total 2816664
drwxr-xr-x 2 root root       4096 mars  22 16:04 ./
drwxr-xr-x 4 root root       4096 févr. 12 09:36 ../
-rw-r--r-- 1 root root 1446797312 mars  22 16:04 i386.img
-rw-r--r-- 1 root root 1437450240 févr. 12 13:45 i386.img.old
On constate qu’une image de sauvegarde a été créée (i386.img.old) afin de pouvoir revenir rapidement en arrière.
 H. Personnalisation des bureaux
Lancer le logiciel menulibre sur le poste de référence (poste1) en utilisant epoptes.

Sur le client, personnalisez les menus
Utilisez l’option « Ne pas afficher dans le menu » 
Création du script
nano bureau.sh

#!/bin/bash

for i in `seq 2 10`
do

echo copy du bureau du poste1 vers le poste $i

rsync -av /home/poste1/.config/xfce4 /home/poste$i/.config/

rsync -av /home/poste2/.config/menus /home/poste$i/.config/

chown -R poste$i:poste$i /home/poste$i/.config/

done
Exécution du script
./bureau.sh

 ./bureau.sh 
copy du bureau du poste1 vers le poste 2
sending incremental file list
xfce4/
xfce4/desktop/
xfce4/panel/
xfce4/xfconf/
xfce4/xfconf/xfce-perchannel-xml/
xfce4/xfwm4/

sent 684 bytes  received 79 bytes  1,526.00 bytes/sec
total size is 23,717  speedup is 31.08
sending incremental file list

sent 115 bytes  received 17 bytes  264.00 bytes/sec
total size is 17,534  speedup is 132.83



